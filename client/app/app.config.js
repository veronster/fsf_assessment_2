// Defines client-side routing
(function () {
    angular
        .module("GMS")
        .config(grocRouteConfig)
    grocRouteConfig.$inject = ["$stateProvider", "$urlRouterProvider"];

    function grocRouteConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state('editWithParam', {
                url: '/edit/:id',
                templateUrl: './app/edit/edit.html',
                controller: 'EditCtrl',
                controllerAs: 'ctrl'
            })
            .state('search', {
                url: '/search',
                templateUrl: './app/search/search.html',
                controller: 'SearchCtrl',
                controllerAs: 'ctrl'
            })
            .state('searchDB', {
                url: '/searchDB',
                templateUrl: './app/search/searchDB.html',
                controller: 'SearchDBCtrl',
                controllerAs: 'ctrl'
            })

        $urlRouterProvider.otherwise("/searchDB");
    }
})();