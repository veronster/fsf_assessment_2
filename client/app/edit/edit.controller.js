(function () {
    'use strict';
    angular
        .module("GMS")
        .controller("EditCtrl", EditCtrl);

    EditCtrl.$inject = ["$filter", "$stateParams", "GrocService"];

    function EditCtrl($filter, $stateParams, GrocService) {

        // Declares the var vm (for ViewModel) and assigns it the object this. Any function or variable that you attach
        // to vm will be exposed to callers of EditCtrl, e.g., edit.html
        var vm = this;

        // Exposed data models -----------------------------------------------------------------------------------------
        // Creates a department object. We expose the department object by attaching it to the vm. This allows us to
        // apply two-way data-binding to this object by using ng-model in our view (i.e., index.html)
        vm.id = "";
        vm.result = {};

        // Exposed functions ------------------------------------------------------------------------------------------
        // Exposed functions can be called from the view.
        vm.initDetails = initDetails;
        vm.search = search;
        vm.toggleEditor = toggleEditor;
        vm.updateGroc = updateGroc;
       
        // Initializations --------------------------------------------------------------------------------------------
        // Functions that are run when view/html is loaded
        initDetails();
        console.log("before state >>>> $stateParams.id", $stateParams.id);
        if($stateParams.id){
            console.log("$stateparams " + $stateParams.id);
            vm.id = $stateParams.id;
            vm.search();
        }


        // Function declaration and definition -------------------------------------------------------------------------

        // Initializes department details shown in view
        function initDetails() {
            console.log("-- show.controller.js > initDetails()");
            vm.result.id = "";
            vm.result.brand = "";
            vm.result.name = "";
            vm.result.upc12 = "";
            vm.showDetails = true;
            vm.isEditorOn = false;
        }

        // Given a department number, this function searches the Employees database for
        // the department name, and the latest department manager's id/name and tenure
        function search() {
            console.log("-- show.controller.js > search()");
//            initDetails();
            vm.showDetails = true;

            GrocService
                .retrieveGrocDB(vm.id)
                .then(function (result) {
                    // Show table structure
                    vm.showDetails = true;

                    // This is a good way to understand the type of results you're getting
                    console.log("-- show.controller.js > search() > results: \n" + JSON.stringify(result.data));

                    // Exit .then() if result data is empty
                    if (!result.data)
                        return;

                    // The result is an array of objects that contain only 1 object
                    // We are assigning value like so, so that we don't have to do access complex structures
                    // from the view. Also this would give you a good sense of the structure returned.
                    // You could, of course, massage data from the back end so that you get a simpler structure
                    vm.result.id = result.data[0].id;
                    vm.result.brand = result.data[0].brand;
                    vm.result.name = result.data[0].name;
                    vm.result.upc12 = result.data[0].upc12;

                    console.log("resultsssss  vm.result.id", vm.result.id,"result.data.id", result.data[0].id);
                    // if (result.data.groceries[0]) {
                    //     vm.result.id = result.data.groceries[0].id;
                    //     vm.result.brand = result.data.groceries[0].brand;
                    //     vm.result.name = result.data.groceries[0].name;
                    //     vm.result.upc12 = result.data.groceries[0].upc12;
                    // }
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > search() > error: \n" + JSON.stringify(err));
                });
        }

        // Switches editor state of the department name input/edit field
        function toggleEditor() {
            vm.isEditorOn = !(vm.isEditorOn);
        }

        // Saves edited department name
        function updateGroc() {
            console.log("-- show.controller.js > save()");
            GrocService
                .updateGroc(vm.result.id, vm.result.brand, vm.result.name, vm.result.upc12)
                .then(function (result) {
                    console.log("-- show.controller.js > save() > results: \n" + JSON.stringify(result.data));
                })
                .catch(function (err) {
                    console.log("--  show.controller.js > save() > error: \n" + JSON.stringify(err));
                });
            vm.toggleEditor();
        }
    }
})();