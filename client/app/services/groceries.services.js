// Always use an IIFE, i.e., (function() {})();
(function () {
    // Attaches DeptService service to the DMS module
    angular
        .module("GMS")
        .service("GrocService", GrocService);

    // Dependency injection. Here we inject $http because we need this built-in service to communicate with the server
    // There are different ways to inject dependencies; $inject is minification safe
    GrocService.$inject = ['$http'];

    // DeptService function declaration
    // Accepts the injected dependency as a parameter. We name it $http for consistency, but you may assign any name
    function GrocService($http) {

        // Declares the var service and assigns it the object this (in this case, the DeptService). Any function or
        // variable that you attach to service will be exposed to callers of DeptService, e.g., search.controller.js
        // and register.controller.js
        var service = this;

        // EXPOSED DATA MODELS -----------------------------------------------------------------------------------------
        // EXPOSED FUNCTIONS -------------------------------------------------------------------------------------------
        service.retrieveGroc = retrieveGroc;
        service.retrieveGrocDB = retrieveGrocDB;
        service.updateGroc = updateGroc;


        // FUNCTION DECLARATION AND DEFINITION -------------------------------------------------------------------------


        // retrieveDept retrieves groceries information from the server via HTTP GET.
        // Parameters: None. Returns: Promise object
        function retrieveGroc() {
            return $http({
                method: 'GET'
                , url: 'api/groceries'
            });
        }

        // retrieveDeptDB retrieves department information from the server via HTTP GET. Passes information via the query
        // string (params) Parameters: searchString. Returns: Promise object
        function retrieveGrocDB(searchString) {
            return $http({
                method: 'GET'
                , url: 'api/groceries'
                , params: {
                    'searchString': searchString
                }
            });
        }

        // updateDept uses HTTP PUT to update department name saved in DB; passes information as route parameters and via
        // HTTP HEADER BODY IMPORTANT! Route parameters are not the same as query strings!
        function updateGroc(id, brand, name, upc12) {
            return $http({
                method: 'PUT'
                , url: 'api/groceries/' + id
                , data: {
                    id: id,
                    brand: brand,
                    name: name,
                    upc12: upc12
                }
            });
        }
    }
})();