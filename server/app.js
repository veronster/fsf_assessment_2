// ----- DEPENDENCIES -----
// load express module
var express = require ('express');

//load helper functions for working with files and directory paths
var path = require('path');

// load helper functions to populate and parse the body of the request object
var bodyParser = require('body-parser');

// load sequelize ORM
var Sequelize = require('sequelize');

//--------- CONTSTANTS ---------
// define server port
const NODE_PORT = parseInt(process.argv[2]) || process.env.APP_PORT || 8080;

// define paths 
const CLIENT_FOLDER = path.join(__dirname + "/../client");
const MSG_FOLDER = path.join(CLIENT_FOLDER + "/assets/messages");

// database credentials
const MYSQL_USERNAME = "root";
const MYSQL_PASSWORD = "root12345";

// create an app instance
var app = express();

// db connection pool
var conn = new Sequelize(
    'groceries',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: "localhost", 
        logging: console.log,
        dialect: 'mysql',
        pool:{
            max: 5,
            min:0,
            idle:1000
        }

    }

);

// import database models into app.js
var Groceries = require ('./models/grocery_list')(conn, Sequelize);

// serve files from public directory
app.use(express.static(CLIENT_FOLDER));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:false}));

// Routes 
// 1. GET /api/groceries - Get all groceries matching criteria
// 2. PUT /api/groceries - Update grocery record


// 1. Get all groceries matching criteria 

app.get("/api/groceries", function(req,res){
    // console.log (req.query.searchString);
    var searchString = req.query.searchString 
    if(searchString){
        Groceries
            .findAll({
                where:{
                    $or:[
                        {id:{$like: "%" + searchString + "%"}},
                        {name:{like: "%" + searchString + "%"}},
                        {brand:{like: "%" + searchString + "%"}}
                    ]
                }
                , order: [["name", "ASC"]],
                  limit: 20
            })
            .then(function(grocery_list){
                res.status(200).json(grocery_list);
            }).catch(function(err){
                res.status(500).json(err);
            })
    } else {
        Groceries
            .findAll({
                order: [["name", "ASC"]],
                limit: 200
            }).then(function(grocery_list){
                res.status(200).json(grocery_list);
            }).catch(function(err){
                res
                    .status(500)
                    .json(err);
            })
    }

});


// 2. Update grocery record 

// app.put(API_GROCERIES_ENDPOINT + "id", function(req,res){
app.put("/api/groceries/:id", function(req,res){
    console.log("============", req.params.id);
    // console.log(req.body);
    var whereClause = {};
    whereClause.id = req.params.id;
    Groceries
        .update({
                upc12:req.body.upc12,
                 brand:req.body.brand,
                 name:req.body.name},
                {where: whereClause}
        ).then(function(result){
            res.status(200).json(result);
        }).catch(function(err){
            res.status(500).json(err);
        });
});
//set up the server
app.set("port", NODE_PORT);

//listen on port
app.listen(app.get("port"), function(){
    console.log("Server started at %s on %d", new Date(), app.get("port"))
});
