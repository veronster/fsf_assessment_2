var Sequelize = require ('sequelize');

const MYSQL_USERNAME = "root";
const MYSQL_PASSWORD = "root12345";

var conn = new Sequelize(
    'groceries',
    MYSQL_USERNAME,
    MYSQL_PASSWORD,
    {
        host: "localhost", 
        logging: console.log,
        dialect: 'mysql',
        pool:{
            max: 5,
            min:0,
            idle:1000
        }

    }

);

conn.authenticate(function(){
    console.log("DB connected");
}).catch(function(){
    console.log(err);
})

// must match /model/filename.js
var employees = require ('./models/grocery_list');


// conn.query("SELECT * FROM EMPLOYEES")
//     .then(function(result){
//         console.log(result)
//     });

// conn.query("SELECT * FROM DEPARTMENTS")
//     .then(function(result){
//         console.log(result)
//     });

// conn.query("SELECT * FROM DEPARTMENTS")
//     .spread(function(result,metadata){
//         console.log(result)
//     });

conn.query("SELECT * FROM GROCERY_LIST")
    .spread(function(result,metadata){
        console.log(result)
    });